const express = require("express");
const exphbs = require("express-handlebars"),
    port = 4444,
    app = express();
const session = require("express-session");
const account = require("./controllers/AccountsC");

app.use(session({
    secret: '123',
    resave: false,
    saveUninitialized: true
}))


//config express handlebar
const hbs = exphbs.create({
    defaultLayout: "layout",
    extname: 'hbs'
})
app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");
//static file
app.use(express.static(__dirname + '/public'));

app.use("/", account);
app.use("/", require("./controllers/categoryC"));
app.use('/', require('./controllers/productC'));


//listen to port 
app.listen(port);
console.log('server running on port 4444');