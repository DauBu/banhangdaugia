const db = require('../utils/db.js');
const tbName = 'Categories';

module.exports = {
    all: async() => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = db.load(sql);
        return rows;
    }
}