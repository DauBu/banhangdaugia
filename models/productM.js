const db = require('../utils/db');
const tbName = 'Products';

module.exports = {
    all: async() => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;

    },
    allByCatId: async(CatId, Page) => {
        const sql = `
        SELECT * FROM ${tbName}
        where ${CatId}=CatID
       LIMIT ${Page},4   `;
        const rows = await db.load(sql);
        console.log(rows);
        return rows;
    },
    getDetail: async(ProId) => {
        const sql = `select * from ${tbName} where ${ProId}=ProID`;
        const rows = await db.load(sql);

        return rows;
    }
}