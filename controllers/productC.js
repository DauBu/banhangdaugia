const express = require('express');
const router = express.Router();
const mCat = require('../models/categoryM');
const mPro = require('../models/productM');

router.get('/:id/detail', async(req, res) => {
    const id = req.params.id;
    const ps = await mPro.getDetail(id);

    res.render('partials/DetailProduct', { title: 'Detail Product', layout: 'detailproduct', ps: ps });

});
module.exports = router;