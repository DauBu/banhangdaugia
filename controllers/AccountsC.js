const express = require('express');
const router = express.Router();
const sha = require("sha.js");
const accountM = require("../models/AccountsM");
const bodyParser = require('body-parser');
const hashLength = 64;

// router.get("/login", (req, res) => {
//     res.redirect('/login');
// })
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));
router.get("/createAccount", function(req, res) {
    res.render("createAccount", { layout: false });
})
router.get("/login", function(req, res) {
    if (req.session.user) res.redirect('/');
    else {
        res.render("login", { layout: false });
    }

})
router.get('/register', (req, res) => {
    res.redirect('/');
})
router.get('/profile', (req, res) => {
    if (!req.session.user) {
        res.redirect('/account/login');
    }
    res.redirect('/');
})
router.post('/login', async(req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const user = await accountM.getByUsername(username);
    console.log(user);
    if (user === null) {
        res.redirect('/login');
        return;
    }

    //xử lý password
    const pwDb = user.f_Password;
    const salt = pwDb.substring(hashLength, pwDb.length);
    const preHash = password + salt;
    const hash = sha('sha256').update(preHash).digest("hex");
    const pwHash = hash + salt;
    //Kiem tra password
    if (pwHash === pwDb) {
        req.session.user = user.f_ID;
        req.session.username = user.f_Username;
        res.redirect('/');
    }
    res.render('login', { error: 'Username or Password uncorrect', layout: false });
    

})

router.post('/createAccount', async(req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    console.log(username + ' ' + password);
    //Xu ly pass word
    const salt = Date.now().toString(16);
    const preHash = password + salt;
    const hash = sha('sha256').update(preHash).digest('hex');
    const pwHash = hash + salt;
    const user = {
        f_ID: null,
        f_Username: username,
        f_Password: pwHash,
        f_Name: req.body.name,
        f_Email: req.body.email,
        f_DOB: req.body.DOB,
        f_Permission: 0
    };
    const uId = await accountM.add(user);
    if (uId) res.redirect('/login');
})

router.get('/profile', (req, res) => {
    if (!req.session.uid) {
        res.redirect('/login');
    } else {
        res.render('account/profile', {
            layout: 'account'
        });
    }
})
router.get('/logout', (req, res) => {
    if (req.session) {
        //delete sesstion object
        req.session.destroy(function(err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        })
    }
})
module.exports = router;