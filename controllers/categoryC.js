const express = require('express');
const router = express.Router();
const mCat = require('../models/categoryM');
const mPro = require('../models/productM');

router.get('/', async(req, res) => {
    const cats = await mCat.all();

    for (let cat of cats) {
        cat.isActive = false;
    }
    cats[0].isActive = true;
    if (req.session.user) res.render("home", {
        login: 1,
        username: req.session.username,
        title: 'cat test',
        cats: cats
    });
    else {
        res.render("home", {
            login: 0,
            title: 'cat test',
            cats: cats
        });
    }


})
router.get('/:id/products', async(req, res) => {
    console.log("in");
    const id = req.params.id;
    const page = 1;

    const cats = await mCat.all();
    const ps = await mPro.allByCatId(id, page);
    console.log(ps);
    for (let cat of cats) {
        if (cat.CatID == id) cat.isActive = true;
        else cat.isActive = false;
    }
    // for (let pro of ps)
    // {
    //     pro.linkImage=`../public/sp/`
    // }
    if (req.session.user) res.render("home", {
        login: 1,
        username: req.session.username,
        cats: cats,
        ps: ps,

    });
    else {
        res.render("home", {
            login: 0,
            cats: cats,
            ps: ps,

        });
    }
})
router.get('/:id/:page/products', async(req, res) => {

    const id = req.params.id;
    const page = req.params.page;

    const cats = await mCat.all();
    const ps = await mPro.allByCatId(id, page);
    for (let cat of cats) {
        if (cat.CatID == id) cat.isActive = true;
        else cat.isActive = false;
    }
    // for (let pro of ps)
    // {
    //     pro.linkImage=`../public/sp/`
    // }
    res.status(200).send({
        title: 'cat test',
        cats: cats,
        ps: ps
    });

})
module.exports = router;